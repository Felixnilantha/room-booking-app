package com.roombooking.controller;

import com.roombooking.model.Booking;
import com.roombooking.model.Room;
import com.roombooking.model.RoomType;
import com.roombooking.service.BookingService;
import com.roombooking.service.RoomService;
import com.roombooking.service.RoomTypeServcie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/booking")
public class BookingController {


    @Autowired
    private RoomTypeServcie roomTypeServcie;

    @Autowired
    private RoomService roomService;

    @Autowired
    private BookingService bookingService;


    @PostMapping("/roomtype/create")
    public void addRoomType(@RequestBody RoomType roomType){

        roomTypeServcie.create(roomType);


    }

    @PostMapping("/room/create")
    public void addRoom(@RequestBody Room room){

        roomService.create(room);


    }

    @PostMapping("/room/book")
    public void createBooking(@RequestBody Booking booking){

        bookingService.createBooking(booking);


    }
    @PutMapping("/room/book")
    public void editBooking(@RequestBody Booking booking){

        bookingService.editBooking(booking);


    }






}
