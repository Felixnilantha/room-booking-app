package com.roombooking.service;

import com.roombooking.model.Guest;
import com.roombooking.repository.GuestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GuestServiceImpl implements GuestService {


    @Autowired
    private GuestRepository gustRepository;


    @Override
    public Guest getGuest(String email) {
        return gustRepository.findGuestByEmail(email);
    }
}
