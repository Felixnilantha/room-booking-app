package com.roombooking.service;

import com.roombooking.model.Booking;
import com.roombooking.model.Guest;
import com.roombooking.model.Room;

public interface BookingService {

    public Booking getBooking(long id);

    public void createBooking(Booking booking);

    public void editBooking(Booking booking);
}
