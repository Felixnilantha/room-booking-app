package com.roombooking.service;

import com.roombooking.model.Room;

public interface RoomService {

    public Room getRoomByRoomType(String roomType);

    public void create(Room room);
}
