package com.roombooking.service;

import com.roombooking.model.RoomType;
import com.roombooking.repository.RoomTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomTypeServiceImpl implements RoomTypeServcie {

    @Autowired
    private RoomTypeRepository roomTypeRepository;
    @Override
    public void create(RoomType roomType) {

        roomTypeRepository.save(roomType);

    }
}
