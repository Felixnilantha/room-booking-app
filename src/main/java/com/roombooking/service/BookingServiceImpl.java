package com.roombooking.service;

import com.roombooking.model.Booking;
import com.roombooking.model.Guest;
import com.roombooking.model.Room;
import com.roombooking.repository.BookingRepository;
import org.springframework.stereotype.Service;

@Service
public class BookingServiceImpl implements BookingService {


    private BookingRepository bookingRepository;
    @Override
    public Booking getBooking(long id) {

        return bookingRepository.findById(id).get();


    }


    @Override
    public void createBooking(Booking booking) {
        bookingRepository.save(booking);

    }

    @Override
    public void editBooking(Booking booking) {
        bookingRepository.save(booking);
    }
}
