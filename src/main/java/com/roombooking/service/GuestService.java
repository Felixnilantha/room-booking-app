package com.roombooking.service;

import com.roombooking.model.Guest;

public interface GuestService {

    public Guest getGuest(String email);

}
