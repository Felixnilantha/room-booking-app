package com.roombooking.service;

import com.roombooking.model.Room;
import com.roombooking.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomRepository roomRepository;


    @Override
    public Room getRoomByRoomType(String roomType) {
        return roomRepository.findRoomByRoomType(roomType);
    }

    @Override
    public void create(Room room) {
        roomRepository.save(room);
    }
}
