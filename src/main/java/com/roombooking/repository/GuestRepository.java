package com.roombooking.repository;

import com.roombooking.model.Guest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuestRepository extends JpaRepository<Guest,Long> {

    public Guest findGuestByEmail(String email);
}
