package com.roombooking.model;

import com.roombooking.repository.BookingRepository;
import com.roombooking.repository.GuestRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class BookingTest {


    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private GuestRepository guestRepository;

    @Before
    public void setUp() throws Exception {
        Guest johnDoe = new Guest();
        johnDoe.setFirstName("John");
        johnDoe.setSurname("Doe");
        johnDoe.setEmail("john.d@gmail.com");

        guestRepository.save(johnDoe);
    }

    @Test
    public void setGuests() {


        assertEquals(guestRepository.findGuestByEmail("john.d@gmail.com").getFirstName(),"John");





    }

    @Test
    public void setRooms() {
    }
}